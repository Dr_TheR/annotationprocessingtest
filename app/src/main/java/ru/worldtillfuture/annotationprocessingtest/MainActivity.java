package ru.worldtillfuture.annotationprocessingtest;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.SayHello;
import com.example.generated.SaysHolder;

@SayHello
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new AlertDialog.Builder(this)
                .setMessage(SaysHolder.getMessage())
                .setTitle("Hello message")
                .create()
                .show();
    }
}
